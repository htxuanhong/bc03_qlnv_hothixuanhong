function layThongTinTuForm() {
  let taiKhoan = document.getElementById("tknv").value;
  let hoTen = document.getElementById("name").value;
  let Email = document.getElementById("email").value;
  let matKhau = document.getElementById("password").value;
  let ngayLam = document.getElementById("datepicker").value;
  let luongCoBan = document.getElementById("luongCB").value;
  let chucVu = document.getElementById("chucvu").value * 1;
  let giolam = document.getElementById("gioLam").value * 1;

  let nhanVien = new NhanVien(
    taiKhoan,
    hoTen,
    Email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    giolam
  );

  return nhanVien;
}

function xuatDanhSachNhanVien(dsnv) {
  contentHTML = "";

  for (var index = 0; index < dsnv.length; index++) {
    let nhanVien = dsnv[index];

    let contentTrTag = `
    <tr>
    <td>${nhanVien.taiKhoan}</td>
    <td>${nhanVien.hoTen}</td>
    <td>${nhanVien.email}</td>
    <td>${nhanVien.ngayLam}</td>
    <td>${
      nhanVien.chucVu === 1
        ? "Giám đốc"
        : nhanVien.chucVu === 2
        ? "Trưởng phòng"
        : nhanVien.chucVu === 3
        ? "Nhân viên"
        : "Chọn chức vụ"
    }</td>
    <td>${nhanVien.tongLuong()}</td>
    <td>${nhanVien.loaiNV()}</td>

    <td>
    <button class="btn btn-success" onclick="suaSinhVien('${
      nhanVien.taiKhoan
    }')" data-toggle="modal"
    data-target="#myModal" >Sửa</button>
    <button class="btn btn-danger" onclick="xoaSinhVien('${
      nhanVien.taiKhoan
    }')">Xóa</button>  
    </td>
    </tr>
    `;

    contentHTML += contentTrTag;
  }

  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function xuatThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCoBan;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}

let reSetForm = function () {
  document.getElementById("formQLNV").reset();
};

document.getElementById("btnTimNV").addEventListener("click", function () {
  let valueName = document.getElementById("searchName").value;

  const ketQuaSerch = danhSachNhanVien.filter((x) =>
    x.loaiNV().includes(valueName)
  );
  xuatDanhSachNhanVien(ketQuaSerch);
});
