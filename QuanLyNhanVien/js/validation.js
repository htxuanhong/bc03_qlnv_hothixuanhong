const validator = {
  kiemTraRong: function (idTaget, idError) {
    let value = document.getElementById(idTaget).value.trim();

    document.getElementById(idError).innerText = "";

    if (!value) {
      document.getElementById(idError).innerText = "Không được để trống";
      return true;
    }

    return false;
  },

  kiemTraTaiKhoan: function (idTarget, idError) {
    let parten = /^[A-Za-z]\w{3,5}$/;

    let valueInPut = document.getElementById(idTarget).value;

    if (parten.test(valueInPut)) {
      document.getElementById(idError).innerText = "";

      return false;
    }

    document.getElementById(idError).innerText = "Tài khoản dài 4-6 ký số";

    return true;
  },
  kiemTraHoTen: function (idTarget, idError) {
    let parten = /[A-Za-z]$/;

    let valueInPut = document.getElementById(idTarget).value;

    if (parten.test(valueInPut)) {
      document.getElementById(idError).innerText = "";

      return false;
    }

    document.getElementById(idError).innerText = "Tên nhân viên phải là chữ";

    return true;
  },

  kiemTraEmail: function (idTarget, idError) {
    let parten = /^[a-z0-9](.?[a-z0-9]){3,}@g(oogle)?mail.com$/;

    let valueInPut = document.getElementById(idTarget).value;

    if (parten.test(valueInPut)) {
      document.getElementById(idError).innerText = "";

      return false;
    }

    document.getElementById(idError).innerText = "Email phải đúng định dạng";

    return true;
  },

  kiemTraMatKhau: function (idTarget, idError) {
    let parten = /(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9]).{6,10}$/;

    let valueInPut = document.getElementById(idTarget).value;

    if (parten.test(valueInPut)) {
      document.getElementById(idError).innerText = "";

      return false;
    }

    document.getElementById(idError).innerText =
      "Mật khẩu có ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt, có 6 - 10 ký tự";

    return true;
  },

  kiemTraChon: function (idTarget, idError) {
    let selectedIndex = document.getElementById(idTarget).selectedIndex;

    if (selectedIndex == 0) {
      document.getElementById(idError).innerText = "Phải chọn chức vụ";

      return true;
    }
    document.getElementById(idError).innerText = "";
    return false;
  },

  kiemTraLuongCB: function (idTaget, idError) {
    let value = document.getElementById(idTaget).value * 1;

    if (value >= 1000000 && value <= 20000000) {
      document.getElementById(idError).innerText = "";

      return true;
    }
    document.getElementById(idError).innerText =
      "Lương cơ bản trong khoảng: 1 000 000 - 20 000 000";
    return false;
  },
  kiemTraSoGio: function (idTaget, idError) {
    let value = document.getElementById(idTaget).value * 1;

    if (value >= 80 && value <= 200) {
      document.getElementById(idError).innerText = "";

      return true;
    }
    document.getElementById(idError).innerText =
      "Số giờ làm trong tháng trong khoảng: 80 - 200 giờ";
    return false;
  },
};

let validationNhanVien = function () {
  const isValidTK = validator.kiemTraRong("tknv", "tbTKNV");

  const isKiemTraTaiKhoan =
    !isValidTK && validator.kiemTraTaiKhoan("tknv", "tbTKNV");

  const isValidHoTen = validator.kiemTraRong("name", "tbTen");

  const isKiemTraHoTen =
    !isValidHoTen && validator.kiemTraHoTen("name", "tbTen");

  const isValidEmail = validator.kiemTraRong("email", "tbEmail");

  const isKTEmail = !isValidEmail && validator.kiemTraEmail("email", "tbEmail");

  const isValidMatKhau = validator.kiemTraRong("password", "tbMatKhau");

  const iskiemTraMK =
    !isValidMatKhau && validator.kiemTraMatKhau("password", "tbMatKhau");

  const isValidNgayLam = validator.kiemTraRong("datepicker", "tbNgay");

  const isValidLuongCB = validator.kiemTraRong("luongCB", "tbLuongCB");

  const isKTLuongCB =
    !isValidLuongCB && validator.kiemTraLuongCB("luongCB", "tbLuongCB");

  const isChonChucVu = validator.kiemTraChon("chucvu", "tbChucVu");

  const isValidGioLam = validator.kiemTraRong("gioLam", "tbGiolam");

  const isKTGioLam =
    !isValidGioLam && validator.kiemTraSoGio("gioLam", "tbGiolam");

  console.log(
    isValidTK,
    isKiemTraTaiKhoan,
    isValidHoTen,
    isKiemTraHoTen,
    isValidEmail,
    isKTEmail,
    isValidMatKhau,
    iskiemTraMK,
    isValidNgayLam,
    isValidLuongCB,
    !isKTLuongCB,
    isChonChucVu,
    isValidGioLam,
    !isKTGioLam
  );

  if (
    isValidTK ||
    isKiemTraTaiKhoan ||
    isValidHoTen ||
    isKiemTraHoTen ||
    isValidEmail ||
    isKTEmail ||
    isValidMatKhau ||
    iskiemTraMK ||
    isValidNgayLam ||
    isValidLuongCB ||
    !isKTLuongCB ||
    isChonChucVu ||
    isValidGioLam ||
    !isKTGioLam
  ) {
    return false;
  }

  return true;
};
