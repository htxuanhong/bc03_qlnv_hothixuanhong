var danhSachNhanVien = [];

const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

const timKiemViTri = function (id, array) {
  return array.findIndex(function (nv) {
    return nv.taiKhoan == id;
  });
};

const luuLoCalStorage = function () {
  // convert array thành json để có thể lưu vào localStorage
  var dsnvJson = JSON.stringify(danhSachNhanVien);

  localStorage.setItem("DSNV_LOCALSTORAGE", dsnvJson);
  console.log(dsnvJson);
};

// lấy dữ liệu từ localstrorage khi user tải lại trang
var dsnvJson = localStorage.getItem("DSNV_LOCALSTORAGE");

// gán cho array gốc và render lại giao diện
if (dsnvJson) {
  danhSachNhanVien = JSON.parse(dsnvJson);

  danhSachNhanVien = danhSachNhanVien.map(function (item) {
    return new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCoBan,
      item.chucVu,
      item.gioLam
    );
  });

  xuatDanhSachNhanVien(danhSachNhanVien);
}

document.getElementById("btnThemNV").addEventListener("click", function () {
  var newNhanVien = layThongTinTuForm();

  var index = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == newNhanVien.taiKhoan;
  });

  // console.log(validationNhanVien());

  if (!validationNhanVien()) {
    return;
  }

  if (index == -1) {
    danhSachNhanVien.push(newNhanVien);
    xuatDanhSachNhanVien(danhSachNhanVien);

    luuLoCalStorage();
  }
  reSetForm();
});

function xoaSinhVien(id) {
  console.log(id);

  let viTri = timKiemViTri(id, danhSachNhanVien);
  console.log({ viTri });

  // xóa tại vị trí tìm thấy với số lượng là 1
  danhSachNhanVien.splice(viTri, 1);
  xuatDanhSachNhanVien(danhSachNhanVien);
  luuLoCalStorage();
}

function suaSinhVien(id) {
  let viTri = timKiemViTri(id, danhSachNhanVien);

  console.log("vi tri", { viTri });

  let nhanVien = danhSachNhanVien[viTri];
  console.log("nhanVien: ", nhanVien);
  xuatThongTinLenForm(nhanVien);
}

document.getElementById("btnCapNhat").addEventListener("click", function () {
  if (!validationNhanVien()) {
    return;
  }

  let nhanVienEdit = layThongTinTuForm();
  console.log(nhanVienEdit);

  let viTri = timKiemViTri(nhanVienEdit.taiKhoan, danhSachNhanVien);

  danhSachNhanVien[viTri] = nhanVienEdit;

  xuatDanhSachNhanVien(danhSachNhanVien);

  luuLoCalStorage();
  reSetForm();
});
